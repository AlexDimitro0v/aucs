TEMPLATES := $(shell find $(SOURCEDIR)text -type f)

all: clean-templates test done

clean-templates:
	@echo "Cleaning templates..."
	@for file in $(TEMPLATES); do \
		[ ! -z $$(tail -c 1 $$file) ] && continue; \
		echo "Fixing $$file"; \
		truncate -s -1 $$file; \
	done

# Done!
done:
	@echo "All done! :)"

# Our pre-commit checks.
pre-commit: test

# First time setup.
setup:
	@echo "First time setup..."
	git config core.hooksPath .githooks

# Tests.
test:
	@echo "Testing..."
